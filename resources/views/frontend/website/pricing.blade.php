<section id="pricing" class="section">
    <div class="container">
      <!-- Start Row -->
      <div class="row">
        <div class="col-lg-12">
          <div class="pricing-text section-header text-center">  
            <div>   
              <h2 class="section-title">Product</h2>
              <div class="desc-text">
                <p>Huong Thi Cosmetics is always committed to bringing you the best quality products.</p>  
                <p>Hương Thị Daisy Flower Extract Platinum.</p>
              </div>
            </div> 
          </div>
        </div>

      </div>
      <!-- End Row -->
      <!-- Start Row -->
      <div class="row pricing-tables">
        @foreach ($products as $item)
        <div class="col-lg-4 col-md-4 col-xs-12">
          <div class="pricing-table text-center">
            <div class="blog-item-wrapper">
              <div class="blog-item-img">
                <a href="{{ route('website.pricing.single', ['slug' => $item->slug]) }}">
                  <img src="{{ $item->thumb }}" class="img-fluid" alt="">
                </a>             
              </div>
            <div class="pricing-details">
              <h3>{{ $item->title }}</h3>
              <h1>{{ number_format($item->price) }}</h1>
              <h3><span>Vnd</span></h3>
              {!! $item->content !!}
            </div>
            <div class="plan-button">
              <a href="#" class="btn btn-border">Purchase</a>
            </div>
          </div>
        </div>
        @endforeach
      </div>
      <!-- End Row -->
      
    </div>
  </section>