<section id="blog" class="section">
    <!-- Container Starts -->
    <div class="container">
      <!-- Start Row -->
      <div class="row">
        <div class="col-lg-12">
          <div class="blog-text section-header text-center">  
            <div>   
              <h2 class="section-title">Customer reviews</h2>
              <div class="desc-text">
                <p>Customer reviews about products</p>  
                
              </div>
            </div> 
          </div>
        </div>

      </div>
      <!-- End Row -->
      <!-- Start Row -->
      <div class="row">
        @foreach ($blogs as $item)
          <!-- Start Col -->
        <div class="col-lg-4 col-md-6 col-xs-12 blog-item">
          <!-- Blog Item Starts -->
          <div class="blog-item-wrapper">
            <div class="blog-item-img">
              <a href="{{ route('website.blog.single', ['slug' => $item->slug]) }}">
                <img src="{{ $item->thumb }}" class="img-fluid" alt="">
              </a>             
            </div>
            <div class="blog-item-text"> 
              <h3><a href="{{ route('website.blog.single', ['slug' => $item->slug]) }}">{{ $item->title }}</a></h3>
              <p>{{ $item->short_description }}</p>
            </div>
            <div class="author">
              <span class="name"><i class="lni-user"></i><a href="#">{{ $item->owner }}</a></span>
              <span class="date float-right"><i class="lni-calendar"></i><a href="#">{{ date('m/d/Y H:i:s', strtotime($item->updated_at)) }}</a></span>
            </div>
          </div>
          <!-- Blog Item Wrapper Ends-->
        </div>
        <!-- End Col -->
        @endforeach
      </div>
      <!-- End Row -->
    </div>
  </section>