<html lang="en" class="nivo-lightbox-notouch">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="keywords" content="Bootstrap, Landing page, Template, Business, Service">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="author" content="Grayrids">
        <title>Demo</title>
        <!--====== Favicon Icon ======-->
        <link rel="shortcut icon" href="{{ URL::asset('asset/img/2.png') }}" type="image/png">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="{{ URL::asset('asset/css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('asset/css/animate.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('asset/css/LineIcons.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('asset/css/owl.carousel.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('asset/css/owl.theme.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('asset/css/magnific-popup.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('asset/css/nivo-lightbox.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('asset/css/main.css') }}">    
        <link rel="stylesheet" href="{{ URL::asset('asset/css/responsive.css') }}">
  </head>
<body>
    @yield('content')
    @include('frontend.template.footer')
    <!-- Go To Top Link -->
    <a href="#" class="back-to-top">
        <i class="lni-chevron-up"></i>
    </a> 

    <!-- Preloader -->
    <div id="preloader">
        <div class="loader" id="loader-1"></div>
    </div>
    <!-- End Preloader -->

    <!-- jQuery first, then Tether, then Bootstrap JS. -->
    <script src="{{ URL::asset('asset/js/jquery-min.js') }}"></script>
    <script src="{{ URL::asset('asset/js/popper.min.js') }}"></script>
    <script src="{{ URL::asset('asset/js/bootstrap.min.js') }}"></script>
    <script src="{{ URL::asset('asset/js/owl.carousel.js') }}"></script>      
    <script src="{{ URL::asset('asset/js/jquery.nav.js') }}"></script>    
    <script src="{{ URL::asset('asset/js/scrolling-nav.js') }}"></script>    
    <script src="{{ URL::asset('asset/js/jquery.easing.min.js') }}"></script>     
    <script src="{{ URL::asset('asset/js/nivo-lightbox.js') }}"></script>     
    <script src="{{ URL::asset('asset/js/jquery.magnific-popup.min.js') }}"></script>     
    <script src="{{ URL::asset('asset/js/form-validator.min.js') }}"></script>
    <script src="{{ URL::asset('asset/js/contact-form-script.js') }}"></script>   
    <script src="{{ URL::asset('asset/js/main.js') }}"></script>
    
</body>
</html>