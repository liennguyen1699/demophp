<header id="home" class="hero-area">
    <div class="overlay">
        <span></span>
        <span></span>
    </div>
    <nav class="navbar navbar-expand-md bg-inverse fixed-top scrolling-navbar">
        <div class="container" >
            <a href="/" class="navbar-brand"><img src="{{ URL::asset('asset/img/logoht.png') }}" alt="">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <i class="lni-menu"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav mr-auto w-100 justify-content-end">
                    <li class="nav-item">
                        <a class="nav-link page-scroll active" href="#home" target="_blade">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link page-scroll" href="#pricing" target="_blade">Product</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link page-scroll" href="#blog">Blog</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="container">
        <div class="row space-100">
            <div class="col-lg-6 col-md-12 col-xs-12">
                <div class="contents">
                    <h2 class="head-title">MỸ PHẨM HƯƠNG THỊ - VIỆT HƯƠNG</h2>
                    <p>"Smile With Vietnamese Perfume - Beauty With Huong Thi  "</p>
                    <div class="header-button">
                        <a href="https://rebrand.ly/slick-ud" rel="nofollow" target="_blank" class="btn btn-border-filled">Purchase Now</a>
                        <a href="https://rebrand.ly/slick-ud" rel="nofollow" target="_blank" class="btn btn-border page-scroll">Learn More</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-xs-12 p-0">
                <div class="intro-img">
                    <img src="{{ URL::asset('asset/img/0000458.jpeg') }}" alt="">
                </div>
            </div>
        </div>
    </div>
</header>