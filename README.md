# Step 1
```
cp .env.example .env
composer install
composer dump-autoload
php artisan key:generate
```

# Step 2:
```
php artisan migrate
php artisan db:seed
php artisan storage:link
```

# Step 3:
```
php artisan serve
```

[Website page](http://127.0.0.1:8000)

[Admin page](http://127.0.0.1:8000/administrator)

```
Account admin :
    username : admin@test.com
    password : 123456
```