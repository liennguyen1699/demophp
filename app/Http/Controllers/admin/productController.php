<?php

namespace App\Http\Controllers\admin;

use Storage;
use Validator;
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use App\Models\products;

class productController extends Controller
{
    public function index(){
        if (isset($_GET['page']) && is_numeric($_GET['page'])) {
            $page_current = intval($_GET['page']);
            if ($page_current >= 1) {
                Paginator::currentPageResolver(function () use ($page_current) {return $page_current;});
            }
        }
        $recode = 10;
        $products = products::orderByDesc('id')->paginate($recode);
        return view('admin.product.index', ['data' => $products]);
    }

    public function deleteProduct($id) {
        $product = products::find($id);
        if(is_null($product)) {
            return redirect()->route('product.index')->with('error', 'Không tìm thấy sản phẩm');
        }

        if($product->delete()) return redirect()->route('product.index')->with('success', 'Xóa thành công');
        return redirect()->route('product.index')->with('success', 'Xóa thất bại');
    }

    private function validator($request) {
        $rules = [
            'title' => 'required|min:3|max:250',
            'price' => 'required|numeric|min:0',
            'content' => 'required|min:3|max:10000',
        ];
        $messages = [
            'title.required' => 'Tiêu đề de là trường bắt buộc',
            'title.min' => 'Tiêu đề phải chứa ít nhất 3 ký tự',
            'title.max' => 'Tiêu đề có nhiều nhất 250 ký tự',
            'price.required' => 'Trường bắt buộc',
            'price.min' => 'Có giá trị lớn hớn hoặc bằng 0',
            'price.numeric' => 'Phải là số',
            'content.required' => 'Nội dung là trường bắt buộc',
            'content.min' => 'Nội dung chứa ít nhất 3 ký tự',
            'content.max' => 'Nội dung có nhiều nhất 10000 ký tự',
        ];

        if(!empty($request->all()['thumb'])) {
            $rules['thumb'] = 'image|mimes:jpeg,png,jpg,gif|max:2048';
            $messages['thumb.image'] = 'Bạn cần chọn hình ảnh';
            $messages['thumb.mimes'] = 'Hình ảnh không đúng vơi định dạng';
            $messages['thumb.max'] = 'Hình ảnh quá lớn';
        }

        return Validator::make($request->all(), $rules, $messages);
    }

    public function create(){
        return view('admin.product.create');
    }

    public function createPost(Request $request){
        $validator = $this->validator($request);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $new = new products();
        if($request->hasFile('thumb')) {
            $new->thumb = Storage::url(Storage::disk('public')->put('image', $request->file('thumb')));
        }

        $slug = Str::slug($request->all()['title'], '-');
        $dem = 0;
        $exist = false;
        do
        {
            $exist = products::where('slug', $slug)->exists();
            if($exist) {
                $dem++;
                $slug .= '-'.$dem;
            }
        }
        while($exist);

        $new->title = $request['title'];
        $new->slug = $slug;
        $new->is_show = !is_null($request->input('is_show'));
        $new->price = $request['price'];
        $new->content = $request['content'];
        if($new->save()) return redirect()->route('product.create')->with('success', 'Thêm thành công');

        return redirect()->back()->withInput()->with('error', 'Thêm thất bại');
    }

    public function edit($id){
        $product = products::find($id);
        if(is_null($product)) {
            return redirect()->route('product.index')->with('error', 'Không tìm thấy sản phẩm');
        }

        return view('admin.product.create', [
            'thumbnail' => $product->thumb,
            'title' => $product->title,
            'price' => $product->price,
            'is_show' => $product->is_show,
            'content' => $product->content
        ]);
    }

    public function editPost($id, Request $request) {
        $validator = $this->validator($request);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $product = products::find($id);
        if(is_null($product)) {
            return redirect()->route('product.index')->with('error', 'Không tìm thấy sản phẩm');
        }
        if($request->hasFile('thumb')) {
            $product->thumb = Storage::url(Storage::disk('public')->put('image', $request->file('thumb')));
        }

        $slug = Str::slug($request->all()['title'], '-');
        $dem = 0;
        $exist = false;
        do
        {
            $exist = products::where('slug', $slug)->exists();
            if($exist) {
                $dem++;
                $slug .= '-'.$dem;
            }
        }
        while($exist);

        $product->title = $request['title'];
        $product->slug = $slug;
        $product->is_show = !is_null($request->input('is_show'));
        $product->price = $request['price'];
        $product->content = $request['content'];
        if($product->save()) return redirect()->route('product.index')->with('success', 'Thêm thành công');

        return redirect()->back()->withInput()->with('error', 'Thêm thất bại');
    }
}
